# AlgoritmosNarrativos

Uno de los programas transversales de la nueva edición DONE de Foto Colectania es Un día en tu vida: algoritmos narrativos, que está impulsado por Julián Barón.

El proyecto puede entenderse como el sustrato de una nueva fase del programa educativo imagenred.org y que consiste en una serie de ejercicios o acciones visuales que pretende hacernos reflexionar sobre nuestra relación con las imágenes desde sus lógicas de producción, gestión, circulación, consumo y apropiación.

El objetivo ahora es propiciar una reflexión sobre la condición creadora en el entorno de la inteligencia artificial, para plantear un nuevo ejercicio de imagenred.org, el número 9. Para ello, se enfocará la atención en los procesos que definen y establecen los algoritmos como unidad básica de definición y desarrollo de la mayor parte de nuestro ecosistema digital, presente en nuestro día a día sin que muchas veces seamos conscientes de ello.

Un día en tu vida: algoritmos narrativos pretende servir de punto de encuentro para la reflexión y la investigación de nuevos modos de creación y agitación visual a través de la puesta en marcha de una serie de acciones, tareas y procesos que puedan ir generando una conciencia social colectiva alrededor del propio fenómeno de la inteligencia artificial.

Para este nuevo ejercicio se contará con la participación de las Escuelas Amigas de Foto Colectania y para ello se está diseñando una web que tiene alma de laboratorio. Esta servirá de herramienta para compartir y entender con los diferentes grupos de estudiantes cuáles son los mecanismos que utilizan los algoritmos y empezar así a “jugar” con la máquina.
